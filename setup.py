#!/usr/bin/env python

from setuptools import find_packages, setup

setup(
    name='dev-cli',
    version='0.1.0',
    description='Cli app for development workflow',
    author='Andrew Kreuzer',
    author_email='akreuzer@mobials.com',
    packages=find_packages(),
    entry_points={
        'console_scripts': [ 'dev=dev_cli:cli']
    },
    install_requires=[
        'click',
        'pyyaml',
        'gitpython',
    ],
    data_files=[ ('dev-cli', ['config/config.yml'])],
)
