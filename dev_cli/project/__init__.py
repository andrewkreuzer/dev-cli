import sys

import click
import git
import json
from pathlib import Path

from dev_cli.docker import connect

class Project():

    def __init__(self, project, info):
        self.project_name = project
        self.directory = info.get('directory', None)
        self.repository   = info.get('repository', None)

    def init(self, ctx, build, clone=True):
        if self.directory:
            print(f"this project is already initialized at {self.directory}")
            return
        else:
            self.directory = str(Path().absolute())
            ctx.obj['config'].save()


        if clone:
            print(f"cloning {self.project_name} from {self.repository}")
            try:
                git.Repo.clone_from(
                        self.repository,
                        f'{self.project_name}',
                        # progress=pbar.update
                )
                self.directory = str(Path().absolute().joinpath(self.project_name))
                ctx.obj['config'].save()

            except git.GitCommandError as e:
                if 'already exists' in str(e):
                    print("Directory already exists, has the project already been initialized?")

        if build:
            self.build(ctx)

    def build(self, ctx):
        print("building docker image")
        logs = connect().api.build(
                path=self.directory,
                dockerfile=self.dockerfile,
                tag=self.project_name,
                rm=True,
                forcerm=True,
        )

        #TODO: refactor this is a little messy
        log_loop = []
        for log in logs:
            with open("logs.txt", "a") as f:
                f.write(str(log))
                f.write('\n')

            out = json.loads(log)
            if 'stream' in out:
                out = out['stream'].replace('\n', '')

            if len(log_loop) == 5:
                sys.stdout.write(u"\u001b[1000D") # Move left
                sys.stdout.write(u"\u001b[" + str(5) + "A") # Move up

                for l in log_loop:
                    click.echo(u"\u001b[K" + click.style(str(l), fg='blue'))

                log_loop.pop(0)
                log_loop.append(out)

            if len(log_loop) < 5:
                print(log)
                log_loop.append(log)

        print(f"{self.project_name} docker image built")

    def update(self, ctx, repo):
        self.repository = repo

        ctx.obj['config'].save()

    def dump(self):
        return {
            'repository': self.repository,
            'directory': self.directory
        }

class Projects():

    def __init__(self):
        self.list = []

    def search(self, project_name):
        for p in self.list:
            if p.project_name == project_name:
                return p

        return None

    def append(self, project):
        if type(project) != Project:
            raise TypeError("Provided project is not of type Project")

        self.list.append(project)

    def remove(self, project):
        self.list.remove(project)

    def get_all(self):
        return self.list
