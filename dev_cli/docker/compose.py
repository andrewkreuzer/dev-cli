from pathlib import Path
import subprocess

import yaml

class ComposeConfig():

    def __init__(self, compose_file):
        self.compose_file = compose_file
        self.compose_object = None
        self.version = None
        self.services = {}

    def load(self):
        with open(self.compose_file) as f:
            self.compose_object = yaml.load(f, Loader=yaml.FullLoader)

        self.version = self.compose_object['version']
        for service, info in self.compose_object['services'].items():
            self.services['service'] = Service(
                    name=service,
                    image=info.get('image', None),
                    build=info.get('build', None),
                    ports=info.get('ports', None),
                    volumes=info.get('volumes', None),
                )

    def run_compose(self):
        subprocess.run(["docker-compose", "up", "-d"])


class Service():

    def __init__(self, name, build, image, ports, volumes):
        self.name = name
        self.build = build
        self.image = image
        self.ports = ports
        self.volumes = volumes


if __name__ == '__main__':
    path = str(Path.home().joinpath('dev/python/dev-cli/test/docker-compose.yml'))
    config = ComposeConfig(path)
    config.load()
    config.run_compose()
