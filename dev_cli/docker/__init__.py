import docker

def connect():
    return docker.from_env()


if __name__ == '__main__':
    connect()
