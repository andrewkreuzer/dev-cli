import click

from dev_cli.config import Config
from dev_cli.project import Project
from dev_cli.docker.compose import *

@click.group()
@click.option('--debug', is_flag=True)
@click.pass_context
def cli(ctx, debug):
    config = Config()
    if debug:
        config.debug = True

    ctx.ensure_object(dict)
    ctx.obj['config'] = config

def project_search(ctx, param, value):
    if value == 'all':
        return value

    if not value:
        value = Path().cwd().stem

    if (project_obj := ctx.obj['config'].projects.search(value)):
        return project_obj

    click.echo(f"{value} is not in projects, initialize it using `dev init`")
    ctx.exit()

@cli.command()
@click.argument('project', required=False)
@click.option('--build/--no-build', default=False)
@click.pass_context
def init(ctx, project, build):

    if project == 'all':
        for p in ctx.obj['config'].projects.get_all():
            p.init(ctx, build)
        return

    cwd_project = False
    if not project:
        project = Path().cwd().stem
        cwd_project = True

    if (project_obj := ctx.obj['config'].projects.search(project)):
        project_obj.init(ctx, build, clone=not cwd_project)
        return

    if click.confirm(
        f"{project} was not found, would you like to add it?",
        default=True
    ):
        repo = click.prompt("Repo")

        project = Project(
                project, {
                    'repository': repo,
                    'directory': str(Path().cwd())
                }
        )

        project.init(ctx, build, clone=not cwd_project)

        ctx.obj['config'].projects.append(project)
        ctx.obj['config'].save()


@cli.command()
@click.argument('project', required=False, callback=project_search)
@click.pass_context
def build(ctx, project):

    if project == 'all':
        for p in ctx.obj['config'].projects.get_all():
            p.build(ctx)
        return

    if project:
        project.build(ctx)

@cli.command()
@click.argument('project', required=False, callback=project_search)
@click.pass_context
def info(ctx, project):

    if project:
        print(project.dump())

@cli.command()
@click.argument('project', required=False, callback=project_search)
@click.option('-r', '--repository', 'repo', prompt=True)
@click.pass_context
def update(ctx, project, repo):

    if project:
        project.update(ctx, repo)

@cli.command()
@click.argument('project', required=False, callback=project_search)
@click.pass_context
def remove(ctx, project):

    if project:
        ctx.obj['config'].projects.remove(project)
        ctx.obj['config'].save()

@cli.command()
@click.pass_context
def projects(ctx):

    for p in ctx.obj['config'].projects.get_all():
        print(p.project_name)

if __name__ == '__main__':
    cli()
