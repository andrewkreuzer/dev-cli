from pathlib import Path
import site

import yaml

from dev_cli.project import *

class Config():

    def __init__(self):
        self.global_config_path = Path(site.USER_BASE + '/dev-cli/config.yml')
        self.global_config = None
        self.cwd_config = None
        self.projects = Projects()
        self.debug = False

        if self.global_config_path.is_file():
            self.global_config = self.load(self.global_config_path)

            for p, info in self.global_config['projects'].items():
                self.projects.append(Project(p, info))

        cwd_config_file = Path().absolute().joinpath('config.yml')
        if cwd_config_file.is_file():
            self.cwd_config = self.load(cwd_config_file)

            # for p, info in self.cwd_config['projects'].items():
            #     if not self.projects.search(p):
            #         self.projects.append(Project(p, info, False))

    def load(self, path):
        with open(path, 'r') as f:
            return yaml.load(f, Loader=yaml.FullLoader)

    def save(self):
        projects = {'projects': {}}
        for p in self.projects.get_all():
            projects['projects'][p.project_name] = p.dump()

        with open(self.global_config_path, 'w') as f:
            f.seek(0)
            yaml.dump(projects, f)
            f.truncate()

if __name__ == "__main__":
    t = Config()
    t.save()
